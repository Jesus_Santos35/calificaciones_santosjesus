from django.shortcuts import render
from customers.models import Customers

# Create your views here.


def index(request):
    customers = Customers.objects.all()
    return render(request, 'index.html', {'customers': customers})
