"""soa_services URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from soa_services import views
from customers import views as cv
from calificaciones import views as ca

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', views.say_hi),
    path('customers/', cv.index),
    path('calif/', ca.index)
    # path('customers/', include('customers.urls')),
]
