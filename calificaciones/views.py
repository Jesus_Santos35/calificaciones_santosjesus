from django.shortcuts import render
from calificaciones.models import Subjects, Califications

# Create your views here.


def index(request):
    subjects = Subjects.objects.all()
    califications = Califications.objects.all()
    return render(request, 'materias.html', {'subjects': subjects, 'califications': califications})
