from django.db import models

# Create your models here.


class Student(models.Model):
    name = models.CharField('nombre', max_length=30)
    last_name = models.CharField('apellidos', max_length=50)


class Subjects(models.Model):
    subject = models.CharField('materia', max_length=40)


class Califications(models.Model):
    subject = models.CharField('materia', max_length=40)
    student = models.CharField('Estudiante', max_length=80)
    calif_1 = models.DecimalField(
        'calificacion 1', max_digits=5, decimal_places=1)
    calif_2 = models.DecimalField(
        'calificacion 2', max_digits=5, decimal_places=1)
    calif_3 = models.DecimalField(
        'calificacion 3', max_digits=5, decimal_places=1)
